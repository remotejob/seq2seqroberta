conda activate fidistillationsv2 && unset LIBTORCH && unset LD_LIBRARY_PATH 

python savetokenizeddataset.py

export KAGGLE_CONFIG_DIR=/home/juno/kagglegregtrettit
cp kdatasets/dataset-metadata.gretrettit.json ~/common/seq2seq/distilrobertabigtokenizeddatasets/dataset-metadata.json
kaggle datasets create -r zip -p ~/common/seq2seq/distilrobertabigtokenizeddatasets/ 
kaggle datasets version -r zip -p ~/common/seq2seq/distilrobertabigtokenizeddatasets/ -m "Updated data 0"


cp kernelgpu/kernel-metadata.gregtreffit.json  kagglerun/kernel-metadata.json

kaggle kernels push -p kagglerun/



rm -rf lastoutput && mv output lastoutput && mkdir output
kaggle kernels output gregtrettit/krunv2 -p output/

rm -rf output/models/best_model && rm -rf output/models/runs

cp kdatasets/dataset-metadata.gregtrettit.ckpoint.json output/models/dataset-metadata.json

#kaggle datasets create -r zip -p output/models 
kaggle datasets version -r zip -p output/models -m "Updated data 5"

cp kernelgpu/kernel-metadata.gregtrettit.json kagglerun/kernel-metadata.json && kaggle kernels push -p kagglerun/




